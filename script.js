function openRecipe() {
  let textButton = document.querySelector(".get-started");
  let fullRecipe = document.querySelector(".instructions");

  if (textButton.textContent == "Get Started ➡️") {
    fullRecipe.classList.add("show");
    fullRecipe.classList.remove("not-show");

    textButton.classList.add("opened");
    textButton.classList.remove("closed");
    textButton.innerHTML = "Show Less";
  } else {
    fullRecipe.classList.add("not-show");
    fullRecipe.classList.remove("show");

    textButton.classList.add("closed");
    textButton.classList.remove("opened");

    textButton.innerHTML = "Get Started ➡️";
  }
}

let getstarted = document.querySelector(".get-started");
getstarted.addEventListener("click", openRecipe);
